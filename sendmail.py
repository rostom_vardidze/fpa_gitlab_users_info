import smtplib
from email.mime.text import MIMEText
from argparse import ArgumentParser

parser = ArgumentParser(description="Python script for send email")

parser.add_argument('refName', type=str, help='Branch name')
parser.add_argument('project_path', type=str, help='Project path')
parser.add_argument('commit', type=str, help='Commit author')
parser.add_argument('email_password', type=str, help='Email password')
parser.add_argument('sender', type=str, help='Sender', default="asdu-fpa@so-ups.ru")
parser.add_argument('user', type=str, help='User', default="asdu-fpa")
parser.add_argument('recipients', type=str, help='Recipients')
parser.add_argument('subject', type=str, help='Subject')
parser.add_argument('server', type=str, help='Server', default="ia-ex-nlb.cdu.so")
parser.add_argument('port', type=int, help='Port', default=25)
parser.add_argument('message', type=str, help='Message')


args = parser.parse_args()

refName = args.refName
project_path = args.project_path
commit = args.commit
email_password = args.email_password
sender = args.sender
user = args.user
recipients = [recipients.strip() for recipients in args.recipients.split(',')]
subject = args.subject
server = args.server
port = args.port
message = args.message


def send_email(email_password: str, 
               sender: str, user: str, recipients: list, subject: str, 
               server: str, port: int, message: str) -> bool:
    server = smtplib.SMTP(server, port)
    server.starttls()
    server.login(user, email_password)
    msg = MIMEText(message)
    msg["Subject"] = subject
    server.sendmail(sender, recipients, msg.as_string())

    print("The message was sent successfuly")
    return True


def main():
    send_email(email_password, sender, user, recipients, subject, server, port, message)

if __name__ == "__main__":
    main()
