import gitlab
import csv
from argparse import ArgumentParser

def write_user_info(writer, user, count_projects, count_groups, project_name='None', group_name='None'):
    user_info = {
        'name': user.attributes.get('name', 'None'),
        'email': user.attributes.get('email', 'None'),
        'created_at': user.attributes.get('created_at', 'None'),
        'last_activity_on': user.attributes.get('last_activity_on', 'None'),
        'count_projects': count_projects,
        'count_groups': count_groups,
        'project_name': project_name,
        'group_name': group_name
    }
    writer.writerow(user_info)


parser = ArgumentParser(description="Python script for get information about users in Gitlab")

parser.add_argument('gitlab_url', type=str, help='Gitlab URL. Example: https://gitlab.com')
parser.add_argument('gitlab_token', type=str, help='Gitlab Token: Example: awdijk90w12hlwnabdp912yljwa')


args = parser.parse_args()

URL = args.gitlab_url
TOKEN = args.gitlab_token

# private token or personal token authentication (self-hosted GitLab instance)
gl = gitlab.Gitlab(url=URL, private_token=TOKEN)

# make an API request to create the gl.user object. This is not required but may be useful
# to validate your token authentication. Note that this will not work with job tokens.
gl.auth()


users = gl.users.list(iterator=True)

with open('gitlab_users.csv', mode='w') as csv_file:
    fieldnames = ['name', 'email', 'created_at', 'last_activity_on', 'count_projects', 'count_groups', 'project_name', 'group_name']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    for user in users:
        direct_projects = user.projects.list(get_all=True)
        for project in direct_projects:
            write_user_info(writer, user, len(direct_projects), 'None', project.attributes.get('name', 'None'))

        memberships = user.memberships.list(type='Namespace', get_all=True)
        for membership in memberships:
            group = gl.groups.get(membership.source_id)
            projects = group.projects.list(get_all=True)
            print(f"User {user.attributes.get('name', 'None')} found in Group {group.attributes.get('name', 'None')}")
            for project in projects:
                write_user_info(writer, user, len(projects), len(memberships), project.attributes.get('name', 'None'), group.attributes.get('name', 'None'))
