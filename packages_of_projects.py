import gitlab
import csv
from argparse import ArgumentParser
from datetime import datetime, timezone

fieldnames = ['Проект', 'JavaScript (npm)', 'C# ( nuget)', 
              'Python (pip)', 'Java (maven)', 'PHP (composer)', 
              'Pascal (fppkg)', 'С и С++ (conan)', 'С и С++ (hunter)', 
              'С и С++ (buckaroo)', 'С и С++ (vcpkg)', 'Perl (ppm)']


def is_date_within_one_year(date_str: str) -> bool:
    # Преобразуем строку в объект datetime с учетом часового пояса
    date = datetime.fromisoformat(date_str)

    # Получаем текущую дату и время
    now = datetime.now(timezone.utc)

    # Вычисляем разницу между датами в днях
    diff_days = (now - date).days

    # Если разница в днях меньше 365 (два года), возвращаем True, иначе False
    return diff_days <= 365


def write_language_info(writer, project_name, languages_dict):
    user_info = {
        fieldnames[0]: project_name,
        fieldnames[1]: languages_dict.get('JavaScript', '-'),
        fieldnames[2]: languages_dict.get('C#', '-'),
        fieldnames[3]: languages_dict.get('Python', '-'),
        fieldnames[4]: languages_dict.get('Java', '-'),
        fieldnames[5]: languages_dict.get('PHP', '-'),
        fieldnames[6]: languages_dict.get('Pascal', '-'),
        fieldnames[7]: languages_dict.get('C++', '-'),
        fieldnames[8]: languages_dict.get('C++', '-'),
        fieldnames[9]: languages_dict.get('C++', '-'),
        fieldnames[10]: languages_dict.get('C++', '-'),
        fieldnames[11]: languages_dict.get('Perl', '-')
    }
    writer.writerow(user_info)

parser = ArgumentParser(description="Python script for get information about package managers in Gitlab projects")

parser.add_argument('gitlab_url', type=str, help='Gitlab URL. Example: https://gitlab.com')
parser.add_argument('gitlab_token', type=str, help='Gitlab Token: Example: awdijk90w12hlwnabdp912yljwa')


args = parser.parse_args()

URL = args.gitlab_url
TOKEN = args.gitlab_token

# private token or personal token authentication (self-hosted GitLab instance)
gl = gitlab.Gitlab(url=URL, private_token=TOKEN)

# make an API request to create the gl.user object. This is not required but may be useful
# to validate your token authentication. Note that this will not work with job tokens.
gl.auth()


projects = gl.projects.list(get_all=True)
languages_to_check = ['JavaScript', 'C++', 'C#', 'Java', 'Python', 'PHP', 'Perl', 'Pascal']
total_projects = 0

with open('gitlab_projects.csv', mode='w') as csv_file:
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    for project in projects:
        languages_dict = {}
        project_name = project.attributes.get('name_with_namespace', 'None')
        date = project.attributes.get('last_activity_at', 'None')
        languages = project.languages()
        if is_date_within_one_year(date):
            for key, value in languages.items():
                if key == 'JavaScript' or key == 'TypeScript':
                    languages_dict['JavaScript'] = '+'
                elif key == 'C++' or key == 'C':
                    languages_dict['C++'] = '+'
                elif key in languages_to_check:
                    # Если ключ соответствует одному из языков в списке, добавляем его в словарь
                    languages_dict[key] = '+'
            
            write_language_info(writer, project_name, languages_dict)
            total_projects += 1
            print("Project name", project_name)
            print("Last updated", date)
            print("Languages", languages)

print("THe script finished! Total projects:", total_projects)
