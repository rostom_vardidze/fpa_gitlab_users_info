# GitLab User Information Retrieval Script

This script utilizes the GitLab API to retrieve and export user information to a CSV file. The exported information includes user details, project details, and group details.

## Prerequisites

Make sure you have the required Python packages installed by running:

```bash
pip install -r requirements.txt
```

## How to Run

Execute the script using the following command:

```bash
python python-script GITLAB_URL GITLAB_TOKEN
```

The script will make API requests to GitLab to retrieve information and store it in a CSV file.

## HELP

```bash
python python-script --help
```
