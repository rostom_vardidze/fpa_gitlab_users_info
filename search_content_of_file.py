import gitlab
import csv
from argparse import ArgumentParser

parser = ArgumentParser(description="Python script for get information about Webhooks of Gitlab projects")

parser.add_argument('gitlab_url', type=str, help='Gitlab URL. Example: https://gitlab.com')
parser.add_argument('gitlab_token', type=str, help='Gitlab Token. Example: awdijk90w12hlwnabdp912yljwa')
parser.add_argument('search_text', type=str, help='Text to search in projects. Example: dockerhub')
parser.add_argument('target_file', type=str, help='Target file to search in projects. Example: .gitlab-ci.yml')


args = parser.parse_args()

URL = args.gitlab_url
TOKEN = args.gitlab_token
SEARCH_TEXT = args.search_text
TARGET_FILE = args.target_file
SEARCH_TEXT = SEARCH_TEXT.lower()

# private token or personal token authentication (self-hosted GitLab instance)
gl = gitlab.Gitlab(url=URL, private_token=TOKEN)

# make an API request to create the gl.user object. This is not required but may be useful
# to validate your token authentication. Note that this will not work with job tokens.
gl.auth()


def write_project_info(writer, project_name='None', file_name='None'):
    user_info = {
        'project_name': project_name,
        'file_name': file_name 
    }
    writer.writerow(user_info)


projects = gl.projects.list(iterator=True)
print("Connected to Gitlab")
with open('gitlab_projects_with_dockerhub.csv', mode='w') as csv_file:
    fieldnames = ['project_name', 'file_name']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    
    for project in projects:
        files = []
        project_name = project.attributes.get('name_with_namespace', 'None')
        try:
            files = project.repository_tree(recursive=True, iterator=True)
        except Exception as e:
            print(str(e), "Error getting tree in project:", project_name)
            continue
        for file in files:
            if file.get('type') == 'blob' and file.get('name') == TARGET_FILE:
                id = file['id']
                file_name = file.get('path')
                print("Search in project:", project_name, "file:", file_name)
                file_content = project.repository_raw_blob(id)
                try:
                    decoded_file_content = file_content.decode().lower()
                except UnicodeDecodeError:
                    print("Не удалось декодировать файл:", file_name)
                    continue
                
                if SEARCH_TEXT in decoded_file_content:
                    print("Found in project:", project_name, "file:", file_name)
                    write_project_info(writer, project_name, file_name=file_name)
